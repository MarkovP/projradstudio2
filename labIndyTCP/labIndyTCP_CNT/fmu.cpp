//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm2::acConnectExecute(TObject *Sender)
{
	IdTCPClient1->Host = edHost->Text;
	IdTCPClient1->Port = StrToInt(edPort->Text);
	IdTCPClient1->Connect();
}
//---------------------------------------------------------------------------
void __fastcall TForm2::acDisconnectExecute(TObject *Sender)
{
	IdTCPClient1->Disconnect();
}
//---------------------------------------------------------------------------
void __fastcall TForm2::acGettimeExecute(TObject *Sender)
{
	IdTCPClient1->Socket->WriteLn("time");
	UnicodeString x;
	x = IdTCPClient1->Socket->ReadLn();
    me->Lines->Add(x);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::acGetstrExecute(TObject *Sender)
{
	IdTCPClient1->Socket->WriteLn("str");
	UnicodeString x;
	x = IdTCPClient1->Socket->ReadLn(IndyTextEncoding_UTF8());
	me->Lines->Add(x);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::ActionList1Update(TBasicAction *Action, bool &Handled)
{
	edHost->Enabled = !IdTCPClient1->Connected();
	edPort->Enabled = !IdTCPClient1->Connected();
	acConnect->Enabled = !IdTCPClient1->Connected();
	acDisconnect->Enabled = IdTCPClient1->Connected();
	acGettime->Enabled = IdTCPClient1->Connected();
	acGetstr->Enabled = IdTCPClient1->Connected();
	acGetimage->Enabled = IdTCPClient1->Connected();
}
//---------------------------------------------------------------------------
void __fastcall TForm2::acGetimageExecute(TObject *Sender)
{
	IdTCPClient1->Socket->WriteLn("image");
	TMemoryStream *x = new TMemoryStream();
	try {
		int xSize = IdTCPClient1->Socket->ReadInt64();
		IdTCPClient1->Socket->ReadStream(x, xSize);
		img->Bitmap->LoadFromStream(x);
	}
	__finally  {
		delete x;
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm2::acVersionExecute(TObject *Sender)
{
	IdTCPClient1->Socket->WriteLn("version");
	UnicodeString x;
	x = IdTCPClient1->Socket->ReadLn(IndyTextEncoding_UTF8());
	me->Lines->Add(x);
}
//---------------------------------------------------------------------------
