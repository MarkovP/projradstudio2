//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdCustomTCPServer.hpp>
#include <IdTCPServer.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <FMX.ActnList.hpp>
#include <System.Actions.hpp>
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
	TButton *bu1;
	TButton *bu2;
	TImage *img;
	TLayout *Layout1;
	TEdit *edHost;
	TMemo *me;
	TEdit *edPort;
	TButton *bu3;
	TButton *bu4;
	TButton *bu5;
	TIdTCPClient *IdTCPClient1;
	TActionList *ActionList1;
	TAction *acConnect;
	TAction *acDisconnect;
	TAction *acGettime;
	TAction *acGetimage;
	TAction *acGetstr;
	TButton *Button1;
	TButton *Button2;
	TAction *acVersion;
	TAction *acAutor;
	void __fastcall acConnectExecute(TObject *Sender);
	void __fastcall acDisconnectExecute(TObject *Sender);
	void __fastcall acGettimeExecute(TObject *Sender);
	void __fastcall acGetstrExecute(TObject *Sender);
	void __fastcall ActionList1Update(TBasicAction *Action, bool &Handled);
	void __fastcall acGetimageExecute(TObject *Sender);
	void __fastcall acVersionExecute(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm2(TComponent* Owner);
};

//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
